# Calibration_ABC-RF-SA

[[_TOC_]]

A set of R scripts for the calibration of complex ecological model using the method called **ABC-RF-SA**, based on Approximate Bayesian Computation with Random Forest and Sensitivity Analysis.

This repository is an add-on of:

* paper: Calibration of a complex hydro-ecological model through Approximate Bayesian Computation and
Random Forest combined with sensitivity analysis. F. PICCIONI, C. CASENAVE, M. BARAGATTI, B. CLOEZ, B. VINÇON-LEITE. XXXXX
* dataverse: https://data.inrae.fr/privateurl.xhtml?token=cbc032f1-f0c4-4eac-baa3-6b11f223e9d8

# Installation

First, the user have to clone the repository locally:

```
git clone git@forgemia.inra.fr:simlake/calibration_abc-rf-sa.git
```

The Calibration process requires the R software (version >= 3.6.1) and some R packages:

* sfsmisc
* stats4
* pheatmap
* nprobust
* corpcor
* fda
* abcrf
* RColorBrewer

# Usage

Please take a look at the **readme_gitlab_Calibration_ABC-RF-SA.pdf** user guide. The repository contains 2 folders:

* **Scripts_calib** contains all the scripts useful for the application of the calibration methods described in the paper:

   1. ABC: standard Approximate Bayesian Computation method with rejection algorithm,
   2. ABC-RF: Approximate Bayesian Computation with Random Forest, 
   3. ABC-RF-SA: Approximate Bayesian Computation with Random Forest and Sensitivity Analysis

   The calibration process that is implemented in the scripts of this folder is decomposed into 8 steps:

   * STEP 1: generation of a rds file gathering all the outputs of the simulations
   * STEP 2: computation of the summary statistics and construction of the associated reference table
   * STEP 3: selection of the best simulation among the set
   * STEP 4: sensitivity analysis
   * STEP 5: application of the standard ABC
   * STEP 6: application of the ABC-RF
   * STEP 7: application of the ABC-RF-SA: ABC-RF with prior sensitivity analysis to sort the parameters depending on their impact on the summary statistics
   * STEP 8: post-processing

   **Main_example.R** script is an example that can be used to run the calibration process, from step 1 to 8. It has to be modified to be applied on a concrete example. A version of this script adapted to the Toy model is given in the folder **Toy_model**.

* **Toy_model** contains some scripts to be run to apply the calibration methods on a toy model composed of only two equations and 9 parameters.

   To test the calibration methods ABC, ABC-RF and ABC-RF-SA on the toy model, the user has to run the 3 following R scripts **successively**:

   * 1_Toy_model_simul.R
   * 2_Toy_model_calibration.R
   * 3_Toy_model_plot_results.R

# Authors and acknowledgment

* Francesco Piccioni PhD LEESU ENPC
* Céline Casenave MISTEA INRAE
* Meili Baragatti MISTEA Institut Agro
* Bertrand Cloez MISTEA INRAE
* Brigitte Vinçon-Leite LEESU ENPC
* Max Zinsou Debaly internship

# License
GPL-3 - @celine.casenave INRAE MISTEA 2022

# References

1. Jean-Michel Marin, Louis Raynal, Pierre Pudlo, Christian P. Robert and Arnaud Estoup (2019). abcrf: Approximate Bayesian Computation
  via Random Forests. R package version 1.8.1. https://CRAN.R-project.org/package=abcrf
2. Raynal, L., Marin, J.M., Pudlo, P., Ribatet, M., Robert, C.P., Estoup, A., 2019. ABC random forests for Bayesian parameter inference.
Bioinformatics 35, 1720–1728. doi:10.1093/bioinformatics/bty867. publisher: Oxford Academic.


